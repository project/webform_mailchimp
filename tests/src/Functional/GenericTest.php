<?php

declare(strict_types=1);

namespace Drupal\Tests\webform_mailchimp\Functional;

use Drupal\Tests\system\Functional\Module\GenericModuleTestBase;

/**
 * Generic module test for webform_mailchimp.
 *
 * @group webform_mailchimp
 */
class GenericTest extends GenericModuleTestBase {}
